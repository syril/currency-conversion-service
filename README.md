# currency conversion service

This is an API layer built to demonstrate service to service call in Golang. This is a currency conversion service which calls the
currency-exchange (Java) service to get the currency day rates for the conversion.
