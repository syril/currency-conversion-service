module github.com/syrilster/go-microservice-example

go 1.13

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/gorilla/handlers v1.4.0
	github.com/gorilla/mux v1.7.0
	github.com/opentracing/opentracing-go v1.1.0 // indirect
	github.com/sirupsen/logrus v1.5.0
)
